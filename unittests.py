""" @package tests
    Package which houses unit tests

    UnitTests routine can be called to check that program
    capabilities are performing as expected.
"""

import numpy
import sys, os
from utils import *
from access import *
sys.path.append('./optimization')
import random
import geneticalgorithm
import optimizer


def UnitTests(accessList):
    """ This function executes a set of defined unit tests to verify expected behavior
        of implemented functions is maintained during development. If any test fails
        the program will exit - something is fundamentally broken if this is the case.
        in(AccessList): accessList: the ordered data provided from the problem input file
    """
    utilsTests()
    accessTests(accessList)
    geneticalgorithmTests(accessList)
    optimizerTests()


def utilsTests():
    """ This function executes a set of defined unit tests to verify utils functions
    """
    if exists(None):
        print("Failed exists test 1.")
        sys.exit()

    if not exists(1):
        print("Failed exists test 2.")
        sys.exit()

    range1 = [0 , 1]
    range2 = [0.5, 1.5]
    range3 = [1.1, 2]

    if not intersect(range1, range2):
        print("Failed intersect test 1.")
        sys.exit()

    if intersect(range1, range3):
        print("Failed intersect test 2.")
        sys.exit()

    random.seed(1336)

    seed1 = generateSeed()

    random.seed(seed1)

    seed2 = generateSeed()

    if seed1 is seed2:
        print("Failed generateSeed test 1.")
        sys.exit()

    watchdog = Watchdog(1.0)
    watchdog.start()
    time.sleep(0.5)

    if watchdog.get_time() < 0.5:
        print("Failed watchdog test 1.")
        sys.exit()


def accessTests(accessList):
    """ This function executes a set of defined unit tests to verify access list member
        members were generated as expected
        in(AccessList): accessList: the ordered data provided from the problem input file
    """
    #Should pass
    MinContactTestSuccess = MinContactTest(accessList, [0, 1, 2, 6, 7, 8, 12, 13, 14, 18, 19, 20, 24, 25, 26, 30, 31, 32, 36, 37, 38, 42, 43, 44, 58, 59, 94, 99, 100, 101, 104, 105, 106, 109, 110, 111])

    if not MinContactTestSuccess:
        print("Failed MinContactTest success case unit test, exiting.")
        sys.exit()

    #Should fail
    MinContactTestSuccess = MinContactTest(accessList, [0, 6, 37, 40])

    if MinContactTestSuccess:
        print("Failed MinContactTest failure case unit test, exiting.")
        sys.exit()

    #Should pass
    GroundstationExclusivityTestSuccess = GroundstationExclusivityTest(accessList, [0, 542])

    if not GroundstationExclusivityTestSuccess:
        print("Failed GroundstationExclusivityTest success case unit test, exiting.")
        sys.exit()

    #Should fail
    GroundstationExclusivityTestSuccess = GroundstationExclusivityTest(accessList, [15, 18])

    if GroundstationExclusivityTestSuccess:
        print("Failed GroundstationExclusivityTest failure case unit test, exiting.")
        sys.exit()

    #Should pass
    GroundstationMaxContactTestSuccess = GroundstationMaxContactTest(accessList, [0, 10, 15, 33, 39, 44])

    if not GroundstationMaxContactTestSuccess:
        print("Failed GroundstationMaxContactTest success unit test, exiting.")
        sys.exit()

    #Should fail
    GroundstationMaxContactTestSuccess = GroundstationMaxContactTest(accessList, [0, 10, 15, 33, 39, 44, 229])

    if GroundstationMaxContactTestSuccess:
        print("Failed GroundstationMaxContactTest failure unit test, exiting.")
        sys.exit()

    #Should pass
    SatelliteExclusivityTestSuccess = SatelliteExclusivityTest(accessList, [0, 345])

    if not SatelliteExclusivityTestSuccess:
        print("Failed SatelliteExclusivityTest success unit test, exiting.")
        sys.exit()

    #Should fail
    SatelliteExclusivityTestSuccess = SatelliteExclusivityTest(accessList, [0, 1])

    if SatelliteExclusivityTestSuccess:
        print("Failed SatelliteExclusivityTest failure unit test, exiting.")
        sys.exit()

    if (len(accessList.accessList_antennaSorted_WithGrouping) != accessList.numUniqueGroundstations or
        len(accessList.accessList_antennaSorted) != accessList.numUniqueGroundstations):
        print("Failed antennaSorted failure unit test, exiting.")
        sys.exit()

    if len(accessList.accessList_satelliteSorted) != accessList.numUniqueSatellites:
        print("Failed satelliteSorted failure unit test, exiting.")
        sys.exit()


def geneticalgorithmTests(accessList):
    """ This function executes a set of defined unit tests to verify behavior of the genetic
        algorithm
        in(AccessList): accessList: the ordered data provided from the problem input file
    """
    #disable std out
    sys.stdout = open(os.devnull, 'w')

    ga = geneticalgorithm.GeneticAlgorithm(4, 3, 0.1, 0.1, 1337, accessList)

    ga.evaluateGeneration()

    #enable std out
    sys.stdout = sys.__stdout__

    if len(ga.population) != 8 or len(ga.elite) != 2 or len(ga.parents) != 2:
        print("Failed ga population count unit test, exiting.")
        sys.exit()

    if ga.generation != 1:
        print("Failed ga generation count unit test, exiting.")
        sys.exit()


def optimizerTests():
    """ This function executes a set of defined unit tests to verify behavior of the main 
        optimizer routine
    """
    #disable std out
    sys.stdout = open(os.devnull, 'w')
    try:
        optimizerFunction = optimizer.OptimizerFunction.Random

        #Read and format input file
        optimize = optimizer.Optimizer('sample_ground_accesses.csv', optimizerFunction)

        initial_seed = 13371
        run_time = 0.5 #seconds

        result = optimize.Optimize(run_time, initial_seed)

        optimizerFunction = optimizer.OptimizerFunction.RandomWithGrouping

        #Read and format input file
        optimize = optimizer.Optimizer('sample_ground_accesses.csv', optimizerFunction)

        initial_seed = 13371
        run_time = 0.5 #seconds

        result = optimize.Optimize(run_time, initial_seed)

        optimizerFunction = optimizer.OptimizerFunction.GeneticAlgorithmSingleObjective

        #Read and format input file
        optimize = optimizer.Optimizer('sample_ground_accesses.csv', optimizerFunction)

        optimize.populationSize = 4

        initial_seed = 13371
        run_time = 0.5 #seconds

        result = optimize.Optimize(run_time, initial_seed)

        optimizerFunction = optimizer.OptimizerFunction.GeneticAlgorithmMultiObjective

        #Read and format input file
        optimize = optimizer.Optimizer('sample_ground_accesses.csv', optimizerFunction)

        optimize.populationSize = 4

        initial_seed = 13371
        run_time = 0.5 #seconds

        result = optimize.Optimize(run_time, initial_seed)
    except:
        #enable std out
        sys.stdout = sys.__stdout__
        print("Failed optimizer unit test, exiting.")
        sys.exit()

    #enable std out
    sys.stdout = sys.__stdout__
