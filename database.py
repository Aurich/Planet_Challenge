""" @package database
    The database package is responsible for reporting results to the cloud

    Final results from users executions are reported to the database. These results
    are pulled up on the website www.satellitescheduler.com
"""

from collections import OrderedDict
from urllib.parse import urlencode
import requests
import optimizer
import datetime

def postResults(version, accessList, solution, algorithm):
    """postResults posts the solution (using http get) to the database of www.satellitescheduler.com
        This function requires an internet connection. Additonally, it has a dependency on the 
        requests module.
        in(float): version: the version of the software that is running
        in(AccessList): accessList: the ordered data provided from the problem input file
        in(list[]): solution: the best solution to be posted
        in(OptimizerFunction): algorithm: the algorithm being used
    """ 
    agreement = input("Do you want to upload results to the database? [y/N]")

    if "y" in agreement or "Y" in agreement or "1" in agreement:     

        algorithm = str(algorithm).replace("OptimizerFunction.","")

        user = input("Please input a username: ")
        cost = str(optimizer.CostFunction(accessList, solution, False))
        version = str(version)

        now = datetime.datetime.now()
        date = str(now.strftime("%Y-%m-%d %H:%M"))

        algorithm = str(algorithm)

        satUtils = [0] * accessList.numUniqueSatellites
        satLost  = [0] * accessList.numUniqueSatellites
        satTime  = [0] * accessList.numUniqueSatellites

        start_times = []
        end_times = []

        for i in range(accessList.numUniqueSatellites):
            start_times.append([])
            end_times.append([])

        groundStationNames = []
        groundStationAccessStartTimes = []
        groundStationAccessEndTimes = []
        groundStationAccessSatID = []
        for i in range(accessList.numUniqueGroundstations):
            groundStationNames.append(accessList.accessList_antennaSorted[i][0].antenna)
            groundStationAccessStartTimes.append([])
            groundStationAccessEndTimes.append([])
            groundStationAccessSatID.append([])

        #evaluate J1 (utility) and J2 (lost imagery)
        for index in range(len(solution)):        
            satIndex = accessList[solution[index]].satellite - 1
            groundStationIndex = groundStationNames.index(accessList[solution[index]].antenna)
            start_times[satIndex].append(accessList[solution[index]].start_time)
            end_times[satIndex].append(accessList[solution[index]].end_time)
            groundStationAccessStartTimes[groundStationIndex].append(accessList[solution[index]].start_time)
            groundStationAccessEndTimes[groundStationIndex].append(accessList[solution[index]].end_time)
            groundStationAccessSatID[groundStationIndex].append(accessList[solution[index]].satellite - 1)
            satUtils[satIndex] += accessList[solution[index]].utility
            satLost[satIndex] += accessList[solution[index]].lost_imagery

        for i in range(accessList.numUniqueSatellites):
            min_start_index = start_times[i].index(min(start_times[i]))
            this_start = start_times[i].pop(min_start_index)
            this_end = end_times[i].pop(min_start_index)
            for j in range(len(start_times[i])):
                min_start_index = start_times[i].index(min(start_times[i]))
                next_start = start_times[i].pop(min_start_index)
                next_end = end_times[i].pop(min_start_index)
                satTime[i] += next_start - this_end
                this_start = next_start
                this_end = next_end

        satString = ""
        for i in range(accessList.numUniqueSatellites):
            if i > 0:
                satString += "/"
            satTimeMin = str(int(satTime[i] / 60))
            satTimeSec = str(int(satTime[i] % 60.0))
            satString += str(round(satUtils[i], 3))+','+str(round(satLost[i], 3))+','+satTimeMin+':'+satTimeSec

        gsString = ""
        for i in range(accessList.numUniqueGroundstations):
            if i > 0:
                gsString += "/"
            start_time = groundStationAccessStartTimes[i]
            end_time = groundStationAccessEndTimes[i]
            sat_id = groundStationAccessSatID[i]
            for j in range(len(start_time)):
                if j > 0:
                    gsString += ","
                min_start_index = start_time.index(min(start_time))
                next_start = start_time.pop(min_start_index)
                this_sat = sat_id.pop(min_start_index)
                next_end = end_time.pop(min_start_index)
                timePercent = (float(100.0*next_start))/(float(accessList.maxTime)-float(accessList.minTime))
                gsString += str(int(timePercent))+":"
                timePercent = (float(100.0*next_end))/(float(accessList.maxTime)-float(accessList.minTime))
                gsString += str(int(timePercent))+":"
                gsString += str(this_sat)
           
        paramString = "?v="+version+"&c="+cost+"&d="+date+"&u="+user+"&s="+satString+"&g="+gsString+"&a="+algorithm
        url = 'https://www.satellitescheduler.com/post_data'+paramString

        r = requests.get(url)

        if "true" in str(r.content):
            print("Results posted successfully.")
        else:
            print("Results failed to post.")
    else:
        print("Results not posted.")
