""" @package access
    Package which manages the input file and sorting the satellite accesses as well as determining feasbility

    This package exposes the problem input in a variety of sorted lists which can
    be used for quick and efficient searching and categorizing. The object which is
    being sorted by this package is the Access object which corresponds to one line
    on the provided input file. This package is used to verify if a solution is feasible with the
    PerformTestSuite function.
"""

import csv
from utils import *

class AccessList:
    """Documentation for the AccessList class.

    The Access List is the parent object to a variety of different variants of
    sorted input data. The generic accessList member is all the accesses available
    sorted by index. The accessList_antennaSorted member has all the accesses put into
    individual lists where the index of each list corresponds to the corresponding antenna
    of the access. accessList_satelliteSorted is the same as accessList_antennaSorted except
    it is sorted on the corresponding satellite. accessList_antennaSorted_WithGrouping is a list
    first sorted by which antenna the access corresponds to, followed by grouping into different
    lists dependent on overlapping time of access. satellite_visited_antennas is a list of antennas
    sorted into lists corresponding to the satellite id. This list is used to figure out what antennas
    a specific satellite should be considered a candidate for.
    """

    def __init__(self, filePath, doGrouping):
        """ The constructor.
            in(handle): self: handle of this object
            in(string): filePath: the location of the input file
            in(bool): doGrouping: if true, populates the accessList_antennaSorted_WithGrouping list
        """
        self.filePath = filePath
        self.doGrouping = doGrouping

        self.startTimeStandard = None
        self.startTimeSeconds = None
        self.accessList = []
        self.accessList_antennaSorted = []
        self.accessList_satelliteSorted = []

        self.numUniqueSatellites = 0
        self.numUniqueGroundstations = 0
        self.numAccesses = 0

        #The max utility and lost_imagery variables are used for normalizing cost functions
        self.max_utility = 0
        self.max_lost_imagery = 0
        self.max_time_delta = 0
        self.minTime = 0
        self.maxTime = 0

        self.accessList_antennaSorted_WithGrouping = []
        self.satellite_visited_antennas = []

        #read the input file
        with open(filePath, 'r') as csvfile:
            spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
            for row in spamreader:
                if len(row) == 14:
                    access = Access(row)
                    self.accessList.append(access)

                    self.max_utility = max(access.utility, self.max_utility)
                    self.max_lost_imagery = max(access.lost_imagery, self.max_lost_imagery)

                    #sort by antenna, find list it should be in or add a new one
                    foundList = False
                    for index in range(len(self.accessList_antennaSorted)):
                        if self.accessList_antennaSorted[index][0].antenna == access.antenna:
                            foundList = True
                            self.accessList_antennaSorted[index].append(access)
                            break
                    if not foundList:
                        self.accessList_antennaSorted.append([access])

                    #sort by satellite, find list it should be in or add a new one
                    foundList = False
                    for index in range(len(self.accessList_satelliteSorted)):
                        if self.accessList_satelliteSorted[index][0].satellite is access.satellite:
                            foundList = True
                            self.accessList_satelliteSorted[index].append(access)
                            break
                    if not foundList:
                        self.accessList_satelliteSorted.append([access])
                        self.satellite_visited_antennas.append([access.antenna])

                    if access.antenna not in self.satellite_visited_antennas[access.satellite-1]:
                        self.satellite_visited_antennas[access.satellite-1].append(access.antenna)

        self.numUniqueSatellites = len(self.accessList_satelliteSorted)
        self.numUniqueGroundstations = len(self.accessList_antennaSorted)
        self.numAccesses = len(self.accessList)

        #set the time to a standard starting at zero, in seconds
        minTime = None
        maxTime = None
        for i in range(self.numAccesses):
            times = [StandardTimeToSeconds(self.accessList[i].end_time), StandardTimeToSeconds(self.accessList[i].start_time),
                StandardTimeToSeconds(self.accessList[i].terminator_time), StandardTimeToSeconds(self.accessList[i].midpoint), minTime]
            minTime = min(filter(exists, times))
            maxTime = max(filter(exists, times))

        self.max_time_delta = maxTime - minTime
        self.maxTime = maxTime
        self.minTime = minTime
        
        for i in range(self.numAccesses):
            self.accessList[i].setStartTime(minTime)

        if self.doGrouping:
            self.accessList_antennaSorted_WithGrouping = self.groupAccessPerGroundstation()

    def __getitem__(self, key):
        """ Returns the access at provided index.
            in(handle): self: handle of this object
            in(int): key: the index of the access to return
            return(Access): the Access at the requested index
        """
        return self.accessList[key]

    def __len__(self):
        """ Returns the total number of accesses
            in(handle): self: handle of this object
            return(int): length of the access list (IE: input file rows minus header)
        """
        return len(self.accessList)

    def groupAccessPerGroundstation(self):
        """ groupAccessPerGroundstation groups accesses per ground station based
    		upon overlapping time intervals
            in(handle): self: handle of this object
            return(list[[[Access]]]): the grouped access list, indicies are ground station, then group, then the access
        """
        res = []

        for i in range(self.numUniqueGroundstations):
            groupsForThisGroundstation = []
            accessesForThisGroundstation = self.accessList_antennaSorted[i]
            for j in range(len(accessesForThisGroundstation)):
                ungroupedAccess = accessesForThisGroundstation[j]
                overlapIndicies = []
                for k in range(len(groupsForThisGroundstation)):
                    for m in range(len(groupsForThisGroundstation[k])):
                        groupedAccess = groupsForThisGroundstation[k][m]
                        if intersect([ungroupedAccess.start_time, ungroupedAccess.end_time], [groupedAccess.start_time, groupedAccess.end_time]):
                            overlapIndicies.append(k)
                            break

                if len(overlapIndicies) > 1:
                    print(overlapIndicies)
                    overlapIndicies = overlapIndicies.reverse()
                    print(overlapIndicies)

                newGroup = [ungroupedAccess]
                for k in range(len(overlapIndicies)):
                    newGroup += groupsForThisGroundstation.pop(overlapIndicies[k])

                groupsForThisGroundstation.append(newGroup)
            res.append(groupsForThisGroundstation)

        return res

class Access:
    """Documentation for the Access class. Corresponds to a row in the input file.

    New additions to this class include the start_time, end_time, and terminator_time have
    all been moved to the *_history member variable, and the * variable now holds the time
    since the earliest timestamp in the provided file in seconds. This provides for easier time 
    comparisons.
    """
    def __init__(self, descriptor):
        """ The constructor.
            in(handle): self: handle of this object
            in(string): descriptor: a row from the input file
        """
        self.index = int(descriptor[0])
        self.antenna = descriptor[1]
        self.eclipse_state = descriptor[2]+","+descriptor[3]
        self.end_time = descriptor[4]
        self.id = descriptor[5]
        self.max_el = descriptor[6]
        self.satellite = int(descriptor[7].replace("sat",""))
        self.start_time = descriptor[8]
        self.terminator_time = descriptor[9]
        self.lost_imagery = float(descriptor[10])
        self.midpoint = descriptor[11]
        self.duration = descriptor[12]
        self.utility = float(descriptor[13])
        self.descriptor = descriptor
        self.end_time_history = self.end_time
        self.start_time_history = self.start_time
        self.terminator_time_history = self.terminator_time
        self.midpoint_history = self.midpoint

    def print(self):
        """ Prints this access to the console. Useful for debugging
            in(handle): self: handle of this object
        """
        print("Index: " + self.index)
        print("Antenna: " + str(self.antenna))
        print("Eclipse State: " + self.eclipse_state)
        print("End Time: " + str(self.end_time))
        print("ID: " + str(self.id))
        print("Max El: " + self.max_el)
        print("Satellite: " + str(self.satellite))
        print("Start Time: " + str(self.start_time))
        print("Terminator Time: " + str(self.terminator_time))
        print("Lost Imagery: " + str(self.lost_imagery))
        print("Midpoint: " + str(self.midpoint))
        print("Duration: " + str(self.duration))
        print("Utility: " + str(self.utility))
        print("End Time (Original): " + self.end_time_history)
        print("Start Time (Original): " + self.start_time_history)
        print("Terminator Time (Original): " + self.terminator_time_history)
        print("Midpoint Time (Original): " + self.midpoint_history)

    def setStartTime(self, mintime_seconds):
        """ Sets all the time members relarive to mintime_seconds
            in(handle): self: handle of this object
            in(float): mintime_seconds: time since epoch of earliest time in input file
        """
        self.end_time = StandardTimeToSeconds(self.end_time)
        self.start_time = StandardTimeToSeconds(self.start_time)
        self.terminator_time = StandardTimeToSeconds(self.terminator_time)
        self.midpoint = StandardTimeToSeconds(self.midpoint)

        if self.end_time is None:
            print("error: end_time is none from index "+str(self.index))
        else:
            self.end_time = self.end_time - mintime_seconds

        if self.start_time is None:
            print("error: start_time is none from index "+str(self.index))
        else:
            self.start_time = self.start_time - mintime_seconds

        if self.terminator_time is not None:
            self.terminator_time = self.terminator_time - mintime_seconds

        if self.midpoint is None:
            print("error: midpoint is none from index "+str(self.index))
        else:
            self.midpoint = self.midpoint - mintime_seconds

def PerformTestSuite(accessList, solution, ignoreMinContact):
    """ This function is responsible for evaluating a candidate solution against
        a set of units tests which define solution feasibility
        in(AccessList): accessList: the ordered data provided from the problem input file
        in([int]): solution: the indicies of the solution
        in(bool): ignoreMinContact: if true, the minimum of 3 contacts per satellite test is ignored
        return(bool): true if all tests pass
    """
    MinContactTestSuccess = True if ignoreMinContact else MinContactTest(accessList, solution)

    if not MinContactTestSuccess:
        return False

    GroundstationExclusivityTestSuccess = GroundstationExclusivityTest(accessList, solution)

    if not GroundstationExclusivityTestSuccess:
        return False

    GroundstationMaxContactTestSuccess = GroundstationMaxContactTest(accessList, solution)

    if not GroundstationMaxContactTestSuccess:
        return False

    SatelliteExclusivityTestSuccess = SatelliteExclusivityTest(accessList, solution)

    if not SatelliteExclusivityTestSuccess:
        return False

    return True

def MinContactTest(accessList, solution):
    """ This test checks if all the satellites had their required 3 contacts per day
        This test will pass if all satellites have had a minimum of 3 contacts
        in(AccessList): accessList: the ordered data provided from the problem input file
        in([int]): solution: the indicies of the solution
        return(bool): true if test passes
    """
    accessCountList = []

    for i in range(accessList.numUniqueSatellites):
        accessCountList.append(0)

    for index in solution:
        satIndex = accessList[index].satellite - 1
        accessCountList[satIndex] += 1

    return (min(accessCountList) >= 3)


def GroundstationExclusivityTest(accessList, solution):
    """ This test checks if any satellites are attempting to communicate with a ground
        station at the same time. This also handles the +1 minute downtime after an
        access has been ended.
        This test will pass if no two satellite contacts block eachother at any ground
        station.
        in(AccessList): accessList: the ordered data provided from the problem input file
        in([int]): solution: the indicies of the solution
        return(bool): true if test passes
    """
    groundstationDelayAfterContact = 60.0  # seconds
    groundStationList = []
    accessTimesList = []

    #Get sorted unique list of ground stations
    for index in range(len(accessList)):
        groundStationList.append(accessList[index].antenna)

    groundStationList = list(sorted(set(groundStationList)))

    numberOfGroundStations = accessList.numUniqueGroundstations

    #Create equivalently sized empty list for the access times of our solution
    for index in range(numberOfGroundStations):
        accessTimesList.append([])

    #populate the access times list with solution times
    for index in range(len(solution)):
        thisAccess = accessList[solution[index]]
        thisAccessTimes = [thisAccess.start_time, (thisAccess.end_time + groundstationDelayAfterContact)]
        groundStationIndex = groundStationList.index(thisAccess.antenna)

        #check for overlap, which would spell test failure
        for access_index in range(len(accessTimesList[groundStationIndex])):
            if intersect(thisAccessTimes, accessTimesList[groundStationIndex][access_index]):
                return False

        accessTimesList[groundStationIndex].append(thisAccessTimes)

    return True

def GroundstationMaxContactTest(accessList, solution):
    """ This test checks to make sure no ground stations are being overused.
        This test will pass if all ground stations have 6 or less accesses
        in(AccessList): accessList: the ordered data provided from the problem input file
        in([int]): solution: the indicies of the solution
        return(bool): true if test passes
    """
    MaxContactsPerGroundstation = 6
    groundStationList = []
    accessCountList = []

    #Get sorted unique list of ground stations
    for index in range(accessList.numAccesses):
        groundStationList.append(accessList[index].antenna)

    groundStationList = list(sorted(set(groundStationList)))

    numberOfGroundStations = accessList.numUniqueGroundstations

    #Create equivalently sized empty list for the access times of our solution
    for index in range(numberOfGroundStations):
        accessCountList.append(0)

    #populate the access times list with solution times
    for index in range(len(solution)):
        thisAccess = accessList[solution[index]]
        accessCountList[groundStationList.index(thisAccess.antenna)] += 1
        if accessCountList[groundStationList.index(thisAccess.antenna)] > MaxContactsPerGroundstation:
            return False

    return True

def SatelliteExclusivityTest(accessList, solution):
    """ This test checks to make sure no satellite is attempting to communicate with
        two ground stations at the same time.
        This test will pass if any satellite contacts do not overlap with another
        contact from the same satellite.
        in(AccessList): accessList: the ordered data provided from the problem input file
        in([int]): solution: the indicies of the solution
        return(bool): true if test passes
    """
    accessTimesList = []
    for index in range(accessList.numAccesses):
        accessTimesList.append([])

    for index in solution:
        thisAccess = accessList[index]         
        satIndex = thisAccess.satellite - 1
        entry = [index, thisAccess.start_time, thisAccess.end_time]
        accessTimesList[satIndex].append(entry)

    for index in solution:
        thisAccess = accessList[index]         
        satIndex = thisAccess.satellite - 1
        thisTime = [thisAccess.start_time, thisAccess.end_time]
        indexCount = 0
        for entry in accessTimesList[satIndex]:
            if not index is entry[0]:
                if intersect(thisTime, [entry[1], entry[2]]):
                    return False
            else:
                indexCount += 1
                if indexCount > 1:
                    return False
    return True