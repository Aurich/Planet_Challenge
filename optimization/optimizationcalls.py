""" @package optimizercalls
    Package which holds routines related to optimization

    This package breaks out some of the functionality necessary to test
    optimization cases using random searching methods (not intended for production)
    as well as the capability of producing random initial conditions (which is used
    inside the GA)
"""

import optimizer
from optimizer import *

Generation = collections.namedtuple('Generation', ['solution', 'cost'])
InitialGuessArgs = collections.namedtuple('InitialGuessArgs', ['function', 'filePath', 'seed', 'doGrouping'])

def InitialFeasibleSolutionWrapper(args):
    """InitialFeasibleSolutionWrapper is a wrapper for generating initial feasible solutions
        dependant on the args.function variable (which allows for random guessing with or
        without grouping). This function call is meant to be utilized with multithreading pool
        in(InitialGuessArgs): args: the standardized arguments for generating solutions
        return(Generation): the initial feasible solution
    """
    accessList = AccessList(args.filePath, args.seed)
    return args.function(accessList, args.seed)

def InitialFeasibleSolution(accessList, seed):
    """InitialFeasibleSolution This function produces a feasible
        solution to be used as an initial guess by randomly filling the
        6 available slots per ground station and each time checking against
        the pre-defined rules of the problem.
        in(AccessList): accessList: the ordered data provided from the problem input file
        in(int): seed: the random seed to use for generating the solution
        return(Generation): the initial feasible solution
    """

    random.seed(seed)

    #Current method will start with a random ground station, fill up a slot,
    #and then continue by moving to another random ground station

    accessSlotsPerGroundStation = 6
    minAccessPerSatellite = 3

    allAccessSlots = []

    allSatellites = []

    currentSolution = []

    #fill up list with all the access slots that need to be filled for each groundstation
    for antennaIndex in range(accessList.numUniqueGroundstations):
        for i in range(accessSlotsPerGroundStation):
            allAccessSlots.append(antennaIndex)  

    #fill up list with all the access slots that need to be filled for each satellite
    for satelliteIndex in range(accessList.numUniqueSatellites):
        for i in range(minAccessPerSatellite):
            allSatellites.append(satelliteIndex)  

    #make sure we satisfy the min 3 contact rule
    for i in range(len(allSatellites)):
        random_satellite_index = allSatellites.pop(random.randint(0, len(allSatellites)-1))
        potentialAccesses = accessList.accessList_satelliteSorted[random_satellite_index].copy()
        foundSolution = False
        while len(potentialAccesses) > 0 and not foundSolution:
            popIndex = random.randint(0, len(potentialAccesses)-1)
            thisPotentialSolution = currentSolution + [potentialAccesses.pop(popIndex).index]
            if PerformTestSuite(accessList, thisPotentialSolution, True):
                currentSolution = thisPotentialSolution
                foundSolution = True

    #fill up the rest of the bucket!
    for i in range(len(allAccessSlots)):
        random_antenna_index = allAccessSlots.pop(random.randint(0, len(allAccessSlots)-1))
        potentialAccesses = accessList.accessList_antennaSorted[random_antenna_index].copy()
        foundSolution = False
        while len(potentialAccesses) > 0 and not foundSolution:
            popIndex = random.randint(0, len(potentialAccesses)-1)
            thisPotentialSolution = currentSolution + [potentialAccesses.pop(popIndex).index]
            if PerformTestSuite(accessList, thisPotentialSolution, False):
                currentSolution = thisPotentialSolution
                foundSolution = True

    return Generation(currentSolution, optimizer.CostFunction(accessList, currentSolution, False))

def InitialFeasibleSolutionWithGrouping(accessList, seed):
    """InitialFeasibleSolutionWithGrouping This function produces a feasible
        solution to be used as an initial guess by randomly filling the
        6 available slots per ground station using time-defined groupings of
        potential candidates and each time checking against the pre-defined
        rules of the problem. The implied benefit of this approach is that when
        filling an available access slot of a ground station, we only need to
        look through how many candidates are in an group instead of potentially
        all the possible accesses to fill the slot.
        in(AccessList): accessList: the ordered data provided from the problem input file
        in(int): seed: the random seed to use for generating the solution
        return(Generation): the initial feasible solution
    """
    random.seed(seed)

    #Current method will start with a random ground station, fill up a slot,
    #and then continue by moving to another random ground station

    accessSlotsPerGroundStation = 6
    minAccessPerSatellite = 3

    antennaNames = []

    allAccessSlots = []

    allSatellites = []

    currentSolution = []

    groupList = []

    usedGroupList = []

    #fill up list with all the access slots that need to be filled for each groundstation
    for antennaIndex in range(accessList.numUniqueGroundstations):
        groupList.append([])
        usedGroupList.append([])
        antennaNames.append(accessList.accessList_antennaSorted[antennaIndex][0].antenna)
        for i in range(accessSlotsPerGroundStation):
            allAccessSlots.append(antennaIndex)

    #fill up list with all the access slots that need to be filled for each satellite
    for satelliteIndex in range(accessList.numUniqueSatellites):
        for i in range(minAccessPerSatellite):
            allSatellites.append(satelliteIndex)

    for i in range(accessList.numUniqueGroundstations):
        for j in range(len(accessList.accessList_antennaSorted_WithGrouping[i])):
            groupList[i].append(j)    

    #make sure we satisfy the min 3 contact rule
    for i in range(len(allSatellites)):
        random_satellite_index = allSatellites.pop(random.randint(0, len(allSatellites)-1))
        potentialGroundstations = accessList.satellite_visited_antennas[random_satellite_index].copy()
        foundSolution = False
        while len(potentialGroundstations) > 0 and not foundSolution:
            random_groundstation_index = antennaNames.index(potentialGroundstations.pop(random.randint(0, len(potentialGroundstations)-1)))
            potentialGroups = groupList[random_groundstation_index].copy()
            while len(potentialGroups) > 0 and not foundSolution:
                random_group_index = potentialGroups.pop(random.randint(0, len(potentialGroups)-1))

                #find all indicies of this group for this satellite
                potentialAccessIndexList = []
                for j in range(len(accessList.accessList_antennaSorted_WithGrouping[random_groundstation_index][random_group_index])):
                    if accessList.accessList_antennaSorted_WithGrouping[random_groundstation_index][random_group_index][j].satellite is random_satellite_index+1:
                        potentialAccessIndexList.append(j)

                while len(potentialAccessIndexList) > 0 and not foundSolution:
                    access_index = potentialAccessIndexList.pop(random.randint(0, len(potentialAccessIndexList)-1))
                    thisPotentialSolution = currentSolution + [accessList.accessList_antennaSorted_WithGrouping[random_groundstation_index][random_group_index][access_index].index]
                    if PerformTestSuite(accessList, thisPotentialSolution, True):
                        currentSolution = thisPotentialSolution
                        usedGroupList[random_groundstation_index].append(groupList[random_groundstation_index].pop(groupList[random_groundstation_index].index(random_group_index)))
                        foundSolution = True

    #fill up the rest of the bucket!
    for i in range(len(allAccessSlots)):
        random_antenna_index = allAccessSlots.pop(random.randint(0, len(allAccessSlots)-1))
        potentialGroups = groupList[random_antenna_index].copy()
        foundSolution = False
        while len(potentialGroups) > 0 and not foundSolution:
            random_group_index = potentialGroups.pop(random.randint(0, len(potentialGroups)-1))
            potentialAccesses = accessList.accessList_antennaSorted_WithGrouping[random_antenna_index][random_group_index].copy()
            while len(potentialAccesses) > 0 and not foundSolution:
                random_access = potentialAccesses.pop(random.randint(0, len(potentialAccesses)-1))
                thisPotentialSolution = currentSolution + [random_access.index]
                if PerformTestSuite(accessList, thisPotentialSolution, False):
                    currentSolution = thisPotentialSolution
                    usedGroupList[random_antenna_index].append(groupList[random_antenna_index].pop(groupList[random_antenna_index].index(random_group_index)))
                    foundSolution = True
        if not foundSolution:
            #we will go back and look at old groups...
            potentialGroups = usedGroupList[random_antenna_index].copy()
            while len(potentialGroups) > 0 and not foundSolution:
                random_group_index = potentialGroups.pop(random.randint(0, len(potentialGroups)-1))
                potentialAccesses = accessList.accessList_antennaSorted_WithGrouping[random_antenna_index][random_group_index].copy()
                while len(potentialAccesses) > 0 and not foundSolution:
                    random_access = potentialAccesses.pop(random.randint(0, len(potentialAccesses)-1))
                    thisPotentialSolution = currentSolution + [random_access.index]
                    if PerformTestSuite(accessList, thisPotentialSolution, False):
                        currentSolution = thisPotentialSolution
                        foundSolution = True

    return Generation(currentSolution, optimizer.CostFunction(accessList, currentSolution, False))

def OptimizationIteration_BlindRandom(args):
    """Simplest of a variety of models which will be trialed.
        This is a dumb model which blindly randomly generates the next generation
        and will be used as a baseline to compare with other algorithms. In general,
        this is the functional equivalent of executing the next generation of the GA,
        albeit far less efficient at finding good solutions.
        in(ModelArgs): args: arguments for producing the next generation
        return(Generation): the initial feasible solution  
    """
    random.seed(args.seed)

    gen = InitialFeasibleSolutionWrapper(InitialGuessArgs(InitialFeasibleSolution, args.filePath, generateSeed(), args.doGrouping))

    return gen if (args.previousCost > gen.cost) else Generation(args.previousSolution, args.previousCost)

def OptimizationIteration_RandomWithGrouping(args):
    """This model is only one small step above randomly generating solutions.
        By grouping accesses with similar execution times together for each
        groundstation, we can create feasible solutions by selecting from these
        groups. In general, this function is the functional equivalent of executing
        the next generation of the GA, albeit far less efficient at finding good solutions.
        in(ModelArgs): args: arguments for producing the next generation
        return(Generation): the initial feasible solution 
    """
    random.seed(args.seed)

    gen = InitialFeasibleSolutionWrapper(InitialGuessArgs(InitialFeasibleSolutionWithGrouping, args.filePath, generateSeed(), args.doGrouping))

    return gen if (args.previousCost > gen.cost) else Generation(args.previousSolution, args.previousCost)
