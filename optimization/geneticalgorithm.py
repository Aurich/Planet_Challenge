""" @package geneticalgorithm
    Package which implements a custom GA

    The Genetic Algorithm package is used to optimize a solution
    within the defined problem space of the planet coding challenge.
"""
import random
from optimizationcalls import *
import optimizer
from access import *

Generation = collections.namedtuple('Generation', ['solution', 'cost'])
GAInitialGuessArgs = collections.namedtuple('GAInitialGuessArgs', ['filePath', 'seed', 'varConstraints', 'countPerThread', 'numObjectives'])


class GeneticAlgorithm:
    """Documentation for the genetic algorithm class.

    To utilize the GA, simply instantiate the GeneticAlgorithm class
    and execute the evaluateGeneration routine. Once sufficient
    generations have been processed, extract the final result by
    executing the getBestCandidate routine.
    """

    def __init__(self, populationSize, numObjectives, Pcrossover, Pmutation, seed, accessList):
        """ The constructor.
            in(handle): self: handle of this object
            in(int): populationSize: the size of the population to initialize
            in(int): numObjectives: the number of objectives being optimized over
            in(float): Pcrossover: probability of crossover from 0.0 to 1.0
            in(float): Pmutation: probability of mutation from 0.0 to 1.0
            in(int): seed: seed for random number generation
            in(AccessList): accessList: the ordered data provided from the problem input file
        """
        random.seed(seed)
        self.population = []
        self.parents = []
        self.children = []
        self.fronts = []
        self.elite = []
        self.populationSize = populationSize
        self.varConstraints = ProduceGAArgumentLimits(accessList)
        self.numObjectives = numObjectives
        self.numVars = len(self.varConstraints)
        self.Pcrossover = Pcrossover
        self.Pmutation = Pmutation
        self.accessList = accessList
        self.numParents = max(2, int(0.5 * populationSize))
        self.tournamentK = max(2, int(0.1 * populationSize))
        self.eliteCount = max(2, int(0.1 * populationSize))
        self.generation = 0

        self.initialize()

    def initialize(self):
        """Internally called function to setup initial guesses
        for the first generation.
            in(handle): self: handle of this object
        """

        self.population = []
        self.parents = []
        self.children = []

        #initialize population and evaluate against objective function

        costArray = []

        initial_guess_pool = Pool()

        args = []

        cores = multiprocessing.cpu_count()

        #If debug is true, will load 64 population initial conditions from files at ./debug/*
        debug = False

        if debug:
            for i in range(64):
                with open('./debug/individual_'+str(i)+'.txt', 'r') as myfile:
                    data=myfile.read()
                    data = data.replace("[","")
                    data = data.replace("]","")
                    dataSplit = data.split(",")
                    solution = []
                    for j in range(len(dataSplit)):
                        solution.append(int(dataSplit[j]))
                    individual = Individual(self.varConstraints, self.accessList, False, self.numObjectives)
                    individual.solution = solution
                    individual.solutionToIndividual()
                    individual.calculateObjectiveFunction()
                    costArray.append(individual.objectiveFunction(False))
                    self.population.append(individual)

        else:
            for i in range(cores):
                args.append(GAInitialGuessArgs(self.accessList.filePath, generateSeed(), self.varConstraints, int(self.populationSize/cores), self.numObjectives))

            initial_guess = initial_guess_pool.map(createIndividual, args)
            initial_guess_pool.close()
            initial_guess_pool.join()

            for i in range(len(initial_guess)):
                for j in range(len(initial_guess[i])):
                    costArray.append(initial_guess[i][j].objectiveFunction(False))
                    self.population.append(initial_guess[i][j])

        #sort the population
        newPopArray = []
        while len(costArray) > 0:
            minIndex = costArray.index(min(costArray))
            newPopArray.append(self.population.pop(minIndex))
            costArray.pop(minIndex)

        self.population = newPopArray

        #calculate fronts
        self.fronts = self.calculateFronts(self.population)

        #assign parents by rank
        self.parents = self.tournamentSelection(self.fronts)

        #assign children from crossover and mutation of parents
        for i in range(len(self.parents)):
            self.children.append(self.mutation(self.parents[i]))

        self.children += self.crossover(self.parents)

        print('par: '+str(len(self.parents))+' chi: '+str(len(self.children))+' pop: '+str(len(self.population)))

    def evaluateGeneration(self):
        """Once the GA has been instantiated, this function can be called to
        produce the next generation. This function can be called for n generations.
            in(handle): self: handle of this object
        """
        self.generation += 1

        #   evaluate children against objective function
        #   merge population and children into union
        union = self.population + self.children

        #find the elite and keep them
        self.elite = self.calculateElite(union+self.elite)

        #calculate fronts
        self.fronts = self.calculateFronts(union)

        #assign parents by tournament
        self.parents = self.tournamentSelection(self.fronts)

        #population gets children
        self.population = self.children + self.elite + self.parents

        #children gets CrossoverAndMutation of selected
        self.children = []
        for i in range(len(self.parents)):
            self.children.append(self.mutation(self.parents[i]))

        self.children += self.crossover(self.parents)


    def crossover(self, parents):
        """ This function performs crossover between parentss.
        Crossover is performed by checking each gene against the
        probability of crossover threshold. If crossover is expected
        for a certain gene, the two parents genes are swapped, else
        the genes stay the same for the two children with respect to
        their two parents.
            in(handle): self: handle of this object
            in(list[Individual)]: parents: the list of possible parents
            return(list[individual]): returns a list of two new individuals
        """
        result = []
        parentIDList = []
        for i in range(len(parents)):
            parentIDList.append(i)

        for i in range(int(len(parents)/2)):
            random_parent1 = parents[parentIDList.pop(random.randint(0, len(parentIDList)-1))]
            random_parent2 = parents[parentIDList.pop(random.randint(0, len(parentIDList)-1))]
            child1 = Individual(self.varConstraints, self.accessList, False, self.numObjectives)
            child2 = Individual(self.varConstraints, self.accessList, False, self.numObjectives)
            for j in range(self.numVars):
                if random.random() <= self.Pcrossover:
                    child1.vars[j] = random_parent2.vars[j].copy()
                    child2.vars[j] = random_parent1.vars[j].copy()
                else:
                    child1.vars[j] = random_parent1.vars[j].copy()
                    child2.vars[j] = random_parent2.vars[j].copy()

            child1.individualToSolution(True)
            child2.individualToSolution(True)

            child1.calculateObjectiveFunction()
            child2.calculateObjectiveFunction()

            result.append(child1)
            result.append(child2)
        return result

    def mutation(self, individual):
        """ This function performs mutation on an individual.
        Mutation is performed by checking each gene individually
        against the threshold of probability of mutation. If a gene
        is to mutate, then that gene is randomly reassigned.
            in(handle): self: handle of this object
            in(Individual): individual: the individual to me mutated.
            return(Individual): the mutated individual
        """
        mutated = Individual(self.varConstraints, self.accessList, False, self.numObjectives)
        for i in range(self.numVars):
            if random.random() <= self.Pmutation:
                if random.random() > 0.1:
                    #minor mutation
                    mutated.vars[i] = [individual.vars[i][0], random.random()]  
                else:
                    #catastrophic mutation
                    possibleAccessList = []
                    groundStationID = int(i/6)

                    for j in range(len(self.accessList.accessList_antennaSorted_WithGrouping[groundStationID])):
                        possibleAccessList.append(j)                
                    
                    for j in range(groundStationID*6,(groundStationID*6)+6):
                        try:
                            possibleAccessList.pop(possibleAccessList.index(individual.vars[j][0]))
                        except:
                            pass #Sometimes failed individuals can have 2x of the same possible accesses, we dont really care cause they will die anyway
                    
                    mutated.vars[i] = [possibleAccessList[random.randint(0,len(possibleAccessList)-1)], random.random()]           
            else:
                mutated.vars[i] = individual.vars[i]
        mutated.individualToSolution(True)
        mutated.calculateObjectiveFunction()
        return mutated

    def calculateFronts(self, population):
        """ Calculates fronts based upon objective function values 
            in(handle): self: handle of this object
            in(list[Individual]): population: the population to sort
            return(list[list[Individual}]): the list of fronts
        """
        fronts = []
        if self.numObjectives == 1:
            fronts.append(population)
        else:
            dominations = [0] * len(population)

            #calculate domination counts 
            for i in range(len(population)):
                for j in range(len(population)):
                    if not i == j:
                        for k in range(self.numObjectives):
                            if population[i].objectiveFunction(True)[k] <= population[j].objectiveFunction(True)[k]:
                                dominations[j] += 1

            #put the results into fronts
            for i in range(1,max(dominations)+1):
                thisFront = []

                #put the best one at the front of the first front
                if 0 in dominations and i == 1:
                    thisFront.append(population[dominations.index(0)])
                    dominations[dominations.index(0)] = max(dominations) + 1

                #sort the rest of this front
                while min(dominations) == i:           
                    thisFront.append(population[dominations.index(i)])
                    dominations[dominations.index(i)] = max(dominations) + 1
                if thisFront:
                    fronts.append(thisFront)
        return fronts

    def tournamentSelection(self, fronts):
        """ This function is responsible for determining the parents
        which will mate to produce the next generation. These parents
        are selected by random tournament. For each parent slot available,
        tournamentK potential parents are randomly selected. They battle
        to the death and one parent is victorious. The victorious parent
        is removed from the list and the process is continued until
        numParents parents are found. The parent list is returned and used
        for breeding purposes.
            in(handle): self: handle of this object
            in(list[list[Individual]]): fronts: the sorted population grouped into individual fronts
            return(list[Individual]): the list of victorious parents
        """
        population = []     

        i = 0
        while len(population) <= 2.0*self.numParents and i < len(fronts):
            population += fronts[i]
            i += 1
        selectedParents = []        

        for i in range(self.numParents):
            tournamentGroup = []
            parentGroup = []
            for j in range(len(population)):
                parentGroup.append(j)
            
            for j in range(self.tournamentK):
                potential = random.randint(0,len(parentGroup)-1)
                tournamentGroup.append(parentGroup.pop(potential))
            
            minIndex = tournamentGroup[0]
            minCost = population[tournamentGroup[0]].objectiveFunction(False)

            for j in range(len(tournamentGroup)):
                if population[tournamentGroup[j]].objectiveFunction(False) < minCost:
                    minCost = population[tournamentGroup[j]].objectiveFunction(False)
                    minIndex = tournamentGroup[0]

            selectedParents.append(population.pop(minIndex))

        return selectedParents

    def calculateElite(self, population):
        """ This function selects the elites from a given population.
            The elites are kept between generations such that our best
            and brightest dont accidentally die out during tournament.
            in(handle): self: handle of this object
            in(list[Individual]): population: the population to find elites
            return(list[Individual]): the list of elites
        """

        fitness = []
        elite = []

        for i in range(len(population)):
            fitness.append(population[i].objectiveFunction(False))

        for i in range(self.eliteCount):
            index = fitness.index(min(fitness))
            fitness[index] = 9999
            elite.append(population[index])

        return elite

    def getBestCandidate(self):
        """ This function is meant to be called externally to retrieve the
            results of the best candidate from the genetic algorithm
            in(handle): self: handle of this object
            return(Generation): the results of the best candidate
        """
        targetArray = self.elite
        if len(targetArray) == 0:
            targetArray = self.population
        bestIndividual = targetArray[0]

        for i in range(len(targetArray)):
            if targetArray[i].objectiveFunction(False) < bestIndividual.objectiveFunction(False):
                bestIndividual = targetArray[i]

        return Generation(bestIndividual.individualToSolution(False), bestIndividual.objectiveFunction(False))

    def print(self, printFronts):
        """ Prints all the solutions available in the GA
            in(handle): self: handle of this object
            in(bool): printFronts: if true, prints fronts information for multiobjective, else prints populations
        """
        if printFronts:
            for i in range(len(self.fronts)):
                for member in self.fronts[i]:
                    costs = member.objectiveFunction(True)
                    print(str(i)+','+str(costs[0])+','+str(costs[1]))
        else:
            print("children: ")
            for i in range(len(self.children)):
                print(self.children[i].vars)

            print("population: ")
            for i in range(len(self.population)):
                print(self.population[i].vars)

            print("parents: ")
            for i in range(len(self.parents)):
                print(self.parents[i].vars)

class Individual:
    """Documentation for the Individual class.

    This class is utilized by the GA to keep track of each
    individual solution. Individuals are responsible for their
    genomes and keeping track of their individual fitness with
    respect to the problem's cost function, as well as keeping
    their individual solutions in formats that the GA can use
    and that the user can use.
    """

    def __init__(self, varConstraints, accessList, calculateFeasible, numObjectives):
        """ The constructor.
            in(handle): self: handle of this object
            in(list[[]]): varConstraints: the constraints of the chromosome variables
            in(AccessList): accessList: the ordered data provided from the problem input file
            in(bool): calculateFeasible: if true, calculates a new feasible solution (randomGrouping)
            in(int): numObjectives: the number of objectives being solved
        """
        self.vars = []
        self.solution = None
        self.cost = None
        self.accessList = accessList
        self.numObjectives = numObjectives
        if calculateFeasible:
            self.solution = InitialFeasibleSolutionWithGrouping(self.accessList, generateSeed()).solution
            self.solutionToIndividual()
        else:
            for j in range(len(varConstraints)):
                self.vars.append([random.randint(varConstraints[j][0], varConstraints[j][1]), random.random()]) 
            self.individualToSolution(True)
        self.calculateObjectiveFunction()

    def individualToSolution(self, force):
        """ This function is used to convert the chromosome definition of
            this individual into the standard solution output of access indicies.
            in(handle): self: handle of this object
            in(bool): force: if true, forces recalculation (used for mutation and crossover)
        """
        if self.solution is None or force:

            self.solution = []
            numGroundstations = self.accessList.numUniqueGroundstations
            numSlotsPerGroundstation = 6
            for i in range(numGroundstations):
                antennaSolution = self.vars[i*numSlotsPerGroundstation:(i*numSlotsPerGroundstation)+(numSlotsPerGroundstation)]
                for j in range(numSlotsPerGroundstation):
                    groupIndex = antennaSolution[j][0]
                    satelliteIndex = int(antennaSolution[j][1]*len(self.accessList.accessList_antennaSorted_WithGrouping[i][groupIndex]))
                    self.solution.append(self.accessList.accessList_antennaSorted_WithGrouping[i][groupIndex][satelliteIndex].index)
            self.calculateObjectiveFunction()
        return self.solution

    def solutionToIndividual(self):
        """ This function is used to convert from standard solution format of a
            list of access indicies to chromosome form for the GA
            in(handle): self: handle of this object
        """
        self.vars = []
        slotIndexCount = []
        groundStationName = []
        numSlotsPerGroundstation = 6

        for i in range(self.accessList.numUniqueGroundstations):
            slotIndexCount.append(0)
            groundStationName.append(self.accessList.accessList_antennaSorted_WithGrouping[i][0][0].antenna)
            for j in range(numSlotsPerGroundstation):
                self.vars.append([-1, -1])

        for i in range(len(self.solution)):
            localOffset = groundStationName.index(self.accessList[self.solution[i]].antenna)
            varIndex = (localOffset*numSlotsPerGroundstation) + slotIndexCount[localOffset]
            slotIndexCount[localOffset] += 1
            found = False
            for k in range(len(self.accessList.accessList_antennaSorted_WithGrouping[localOffset])):
                for m in range(len(self.accessList.accessList_antennaSorted_WithGrouping[localOffset][k])):
                    if self.accessList.accessList_antennaSorted_WithGrouping[localOffset][k][m].index == self.solution[i]:
                        found = True
                        self.vars[varIndex] = [k, m/len(self.accessList.accessList_antennaSorted_WithGrouping[localOffset][k])]
                        break

                if found:
                    break

    def calculateObjectiveFunction(self):
        """ calculateObjectiveFunction calculates the cost function for this individual
            in(handle): self: handle of this object
        """
        if not PerformTestSuite(self.accessList, self.solution, False):
            self.cost = [0] * self.numObjectives
            for i in range(self.numObjectives):
                self.cost[i] = random.randint(10000, 100000)
        else:
            self.cost = optimizer.CostFunction(self.accessList, self.solution, True)        

    def objectiveFunction(self, multiobjective):
        """ objectiveFunction returns the cost function for this individual
            in(handle): self: handle of this object
            in(bool): multiobjective: if true, returns the cost in multiobjective form
            return(float): the resulting cost of this individual
        """
        if multiobjective:
            return self.cost
        else:
            return sum(self.cost)/len(self.cost)

def ProduceGAArgumentLimits(accessList):
    """ ProduceGAArgumentLimits calculates the bounds for the genes to be
        used in the GA from the input file.
        in(AccessList): accessList: the ordered data provided from the problem input file
        return(list[[float]]): the list of bounds for the genes
    """
    GAArgs = []

    numSlotsPerGroundstation = 6
    for i in range(accessList.numUniqueGroundstations):
        for j in range(numSlotsPerGroundstation):
            GAArgs.append([0, len(accessList.accessList_antennaSorted_WithGrouping[i])-1])

    return GAArgs

def createIndividual(args):
    """ createIndividual produces args.countPerThread individuals which are
        used to seed the GA as an initial population. This function is meant
        to be called by a multiprocessing pool call and results from multiple
        calls be concatenated at the end of processing.
        in(GAInitialGuessArgs): args: arguments for this thread
        return(list[Individual]): the list of resulting randomly generated individuals
    """
    random.seed(args.seed)
    accessList = AccessList(args.filePath, True)
    res = []
    for i in range(args.countPerThread):
        individual = Individual(args.varConstraints, accessList, True, args.numObjectives)
        individual.objectiveFunction(False)
        res.append(individual)
    return res
    