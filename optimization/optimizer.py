""" @package optimizer
    Package which manages the optimizer which the user shall use to produce solutions

    This package exposes the Optimizer class, which the user shall use to produce solutions. 
    Additionally, the OptimizerFunction enum is used to select HOW the problem shall be solved
    by the optimizer as well as the Cost Function which is used benchmark the fitness of all results.
"""

import random
import collections
import sys, os
from access import *
import multiprocessing
from multiprocessing import Pool
from enum import Enum
import optimizationcalls
import geneticalgorithm
import datetime


ModelArgs = collections.namedtuple('ModelArgs', ['filePath', 'previousSolution', 'previousCost', 'seed', 'doGrouping'])
InitialGuessArgs = collections.namedtuple('InitialGuessArgs', ['function', 'filePath', 'seed', 'doGrouping'])

class OptimizerFunction(Enum):
    """Enum which is used to specify what algorithm the optimizer should use.

    Random: new random feasible solutions are produced every iteration
    RandomWithGrouping: new random feasible solutions are produced every iteration, grouping of 
                        accesses are utilized to reduce search space and spread out solutions
    GeneticAlgorithm: the optimizer will use a genetic algorithm approach
    """
    Random = 1
    RandomWithGrouping = 2
    GeneticAlgorithmSingleObjective = 3
    GeneticAlgorithmMultiObjective = 4

class Optimizer:
    """Optimizer class which is used to solve the provided problem.

    This class is responsible for generating solutions and providing
    the user with the best possible solution in the shortest amount
    of time. This class utilizes the multiprocessing package to spread
    workload across CPU cores, and therefore can utilize a lot of CPU
    resources. To utilize this class, instantiate an Optimizer object,
    call Optimize, and the best result will be returned.
    """

    def __init__(self, filePath, algorithmEnum):
        """ The constructor.
            in(handle): self: handle of this object
            in(string): filePath: the path to the input file
            in(OptimizerFunction): algorithmEnum: determines which function to use to optimize
        """
        self.filePath = filePath
        self.optimizationAlgorithm = algorithmEnum
        self.history = []

        if (algorithmEnum is OptimizerFunction.RandomWithGrouping or 
            algorithmEnum is OptimizerFunction.GeneticAlgorithmSingleObjective or
            algorithmEnum is OptimizerFunction.GeneticAlgorithmMultiObjective):
            self.doGrouping = True
        else:
            self.doGrouping = False

        self.accessList = AccessList(filePath, self.doGrouping)
        self.Pcrossover = 0.05
        self.Pmutation = 0.05
        self.numObjectives = 1
        self.populationSize = 64
        self.targetGeneration = 1

    def Optimize(self, runtime, seed):
        """ Optimize operates for runtime of real time and attempts to minimize the cost function
            using the optimization function specified by optimizationAlgorithm
            in(handle): self: handle of this object
            in(float): runtime: approximately how long we should attempt to optimize for
            in(int): seed: the seed for the entire optimization tree
            return(Generation): the best solution found
        """
        watchdog = Watchdog(runtime)
        watchdog.start()

        if (self.optimizationAlgorithm is OptimizerFunction.GeneticAlgorithmSingleObjective or
            self.optimizationAlgorithm is OptimizerFunction.GeneticAlgorithmMultiObjective):

                if self.optimizationAlgorithm is OptimizerFunction.GeneticAlgorithmMultiObjective:
                    self.numObjectives = 3

                ga = geneticalgorithm.GeneticAlgorithm(self.populationSize, self.numObjectives, self.Pcrossover, self.Pmutation, seed, self.accessList)

                self.history.append([round(watchdog.get_time(), 1), ga.generation, ga.population.copy()])

                ga.evaluateGeneration()
                res = ga.getBestCandidate()                

                while watchdog.check() or ga.generation < self.targetGeneration:
                    ga.evaluateGeneration()
                    res = ga.getBestCandidate()

                    self.history.append([round(watchdog.get_time(), 1), ga.generation, ga.population.copy()])

                    print('at time ' + str(round(watchdog.get_time(), 1)) + ': ' + str(res.cost))
                
                return res
        else:
            if self.optimizationAlgorithm is OptimizerFunction.Random:
                model = optimizationcalls.OptimizationIteration_BlindRandom
                initial_guess_model = optimizationcalls.InitialFeasibleSolution
            elif self.optimizationAlgorithm is OptimizerFunction.RandomWithGrouping:
                model = optimizationcalls.OptimizationIteration_RandomWithGrouping
                initial_guess_model = optimizationcalls.InitialFeasibleSolutionWithGrouping
            else:
                print("Supplied optimizer function not supported")
                sys.exit()

            initial_guess_pool = Pool()
            cores = multiprocessing.cpu_count()
            casesExecuted = 0
            args = []

            random.seed(seed)         

            print("Starting optimization with "+str(cores)+" threads")

            for i in range(cores):
                args.append(InitialGuessArgs(initial_guess_model, self.filePath, generateSeed(), self.doGrouping))

            initial_guess = initial_guess_pool.map(optimizationcalls.InitialFeasibleSolutionWrapper, args)
            initial_guess_pool.close()
            initial_guess_pool.join()
            casesExecuted += 4

            bestIndex = minIndex(initial_guess)

            bestGeneration = initial_guess[bestIndex]

            print('at time ' + str(round(watchdog.get_time(), 1)) + ': ' + str(bestGeneration.cost))

            self.history.append([round(watchdog.get_time(), 1), casesExecuted, bestGeneration])


            while watchdog.check():
                processing_pool = Pool()

                args = []

                for i in range(cores):
                    args.append(ModelArgs(self.filePath, bestGeneration.solution, bestGeneration.cost, generateSeed(), self.doGrouping))

                res = processing_pool.map(model, args)
                processing_pool.close()
                processing_pool.join()
                casesExecuted += 4

                res += [bestGeneration]
                bestIndex = minIndex(res)
                bestGeneration = res[bestIndex]

                print('at time ' + str(round(watchdog.get_time(), 1)) + ': ' + str(bestGeneration.cost))
                self.history.append([round(watchdog.get_time(), 1), casesExecuted, bestGeneration])

            return bestGeneration

    def printOutputToFiles(self, generation):
        """ printOutputToFiles is responsible for printing human readable scheduling documents
            An output file will be written for each ground station and each satellite
            from a provided solution
            in(handle): self: handle of this object
            in(float): solution: the solution to be output
        """

        agreement = input("Do you want to print output files (to results/)? [y/N]")

        if "y" in agreement or "Y" in agreement or "1" in agreement:  
            solution = generation.solution
            cost = generation.cost
            antennaNameList = []

            satAccessIndex = []
            satStartTimes = []
            satStartTimesHistory = []
            satEndTimes = []
            satEndTimesHistory = []
            satGroundStation = []
            satUtility = []
            satLost = []
            satDuration = []

            antennaAccessIndex = []
            antennaStartTimes = []
            antennaStartTimesHistory = []
            antennaEndTimes = []
            antennaEndTimesHistory = []
            antennaSatID = []
            antennaUtility = []
            antennaDuration = []

            for member in self.accessList.accessList_antennaSorted:
                antennaNameList.append(member[0].antenna)
                antennaStartTimes.append([])
                antennaStartTimesHistory.append([])
                antennaEndTimes.append([])
                antennaEndTimesHistory.append([])
                antennaSatID.append([])
                antennaUtility.append([])
                antennaAccessIndex.append([])
                antennaDuration.append([])

            for i in range(self.accessList.numUniqueSatellites):
                satStartTimes.append([])
                satStartTimesHistory.append([])
                satEndTimes.append([])
                satEndTimesHistory.append([])
                satGroundStation.append([])
                satUtility.append([])
                satLost.append([])
                satAccessIndex.append([])
                satDuration.append([])


            for index in solution:
                access = self.accessList[index]
                satID = access.satellite - 1
                antennaID = antennaNameList.index(access.antenna)
                satAccessIndex[satID].append(index)
                satStartTimes[satID].append(access.start_time)
                satStartTimesHistory[satID].append(access.start_time_history)
                satEndTimes[satID].append(access.end_time)
                satEndTimesHistory[satID].append(access.end_time_history)
                satGroundStation[satID].append(access.antenna)
                satUtility[satID].append(access.utility)
                satLost[satID].append(access.lost_imagery)
                satDuration[satID].append(access.duration)

                antennaStartTimes[antennaID].append(access.start_time)
                antennaStartTimesHistory[antennaID].append(access.start_time_history)
                antennaEndTimes[antennaID].append(access.end_time)
                antennaEndTimesHistory[antennaID].append(access.end_time_history)
                antennaSatID[antennaID].append('sat'+str(access.satellite))
                antennaUtility[antennaID].append(access.utility)
                antennaAccessIndex[antennaID].append(index)
                antennaDuration[antennaID].append(access.duration)

            #create the output file path
            now = datetime.datetime.now()
            date = str(now.strftime("%Y-%m-%d_%H_%M_%S"))
            path = "./results/"+date
            os.mkdir(path)

            #write the satellite files
            allUtility = 0
            allLost = 0
            for i in range(self.accessList.numUniqueSatellites):
                outputFile = path + "/sat"+str(i+1)+".txt"
                eventCount = 0
                totalUtility = 0
                totalLost = 0
                with open(outputFile, "w") as output:
                    while len(satAccessIndex[i]) > 0:
                        eventCount += 1
                        minIndex = satStartTimes[i].index(min(satStartTimes[i]))

                        index = satAccessIndex[i].pop(minIndex)
                        start_time = satStartTimes[i].pop(minIndex)
                        start_time_history = satStartTimesHistory[i].pop(minIndex)
                        end_time = satEndTimes[i].pop(minIndex)
                        end_time_history = satEndTimesHistory[i].pop(minIndex)
                        groundstation = satGroundStation[i].pop(minIndex)
                        utility = satUtility[i].pop(minIndex)
                        lost_imagery = satLost[i].pop(minIndex)
                        duration = satDuration[i].pop(minIndex)
                        totalUtility += float(utility)
                        totalLost += float(lost_imagery)
                        outputStr = "event "+str(eventCount)+": access index "+str(index)+"\n"
                        outputStr += "  event start: "+start_time_history+", event end: "+end_time_history+" (duration (s): "+str(duration)+")\n"
                        outputStr += "  connects to "+groundstation+", downlink (MB): "+str(utility)+", lost imagery (MB): "+str(lost_imagery)+"\n"
                        output.write(outputStr)
                        
                    output.write('Total: '+str(eventCount)+' events, downlinked (MB): '+str(totalUtility)+", lost imagery (MB): "+str(totalLost))
                allUtility += totalUtility
                allLost += totalLost

            #write the groundstation files
            for i in range(self.accessList.numUniqueGroundstations):
                outputFile = path + "/"+antennaNameList[i]+".txt"
                eventCount = 0
                totalUtility = 0
                with open(outputFile, "w") as output:
                    while len(antennaAccessIndex[i]) > 0:
                        eventCount += 1
                        minIndex = antennaStartTimes[i].index(min(antennaStartTimes[i]))

                        start_time = antennaStartTimes[i].pop(minIndex)
                        start_time_history = antennaStartTimesHistory[i].pop(minIndex)
                        end_time = antennaEndTimes[i].pop(minIndex)
                        end_time_history = antennaEndTimesHistory[i].pop(minIndex)
                        satellite = antennaSatID[i].pop(minIndex)
                        utility = antennaUtility[i].pop(minIndex)
                        index = antennaAccessIndex[i].pop(minIndex)
                        duration = antennaDuration[i].pop(minIndex)
                        totalUtility += float(utility)
                        outputStr = "event "+str(eventCount)+": access index "+str(index)+"\n"
                        outputStr += "  event start: "+start_time_history+", event end: "+end_time_history+" (duration (s): "+str(duration)+")\n"
                        outputStr += "  connects to "+satellite+", downlink (MB): "+str(utility)+"\n"
                        output.write(outputStr)
                        
                    output.write('Total: '+str(eventCount)+' events, downlinked (MB): '+str(totalUtility))

            #output a generic result file
            outputFile = path + "/result.txt"
            with open(outputFile, "w") as output:
                output.write('cost='+str(cost)+'\nsolution='+str(solution)+'\ntotal downlinked(mb):'+str(allUtility)+"\ntotal lost imagery(mb):"+str(allLost))

            print('finished printing output files')


def CostFunction(accessList, solution, multiobjective):
    """This function evaluates a defined cost function for a given
        solution such that solutions can be compared together for fitness
        in(AccessList): accessList: the ordered data provided from the problem input file
        in(list[int]): solution: the solution to be evaluated
        in(bool): multiobjective: if true, returns multiobjective cost
        return(multiobjective?float:[float]): the fitness (more negative is better)
    """
    #Costs to consider
    #   J1. Amount of data downlinked
    #   J2. Amount of data lost
    #   J3. Amount of time between all communications per satellite
    #
    #   Note: we want to minimize cost, so good things decrease cost,
    #   and bad things increase cost

    cost_weights = [1, 1, 1]
    satellite_weights = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]

    total_satellite_weight = sum(satellite_weights)
    total_cost_weight = sum(cost_weights)

    start_times = []
    end_times = []

    for i in range(accessList.numUniqueSatellites):
        start_times.append([])
        end_times.append([])

    J1 = 0
    J2 = 0
    J3 = 0

    #evaluate J1 (utility) and J2 (lost imagery)
    for index in range(len(solution)):
        satIndex = accessList[solution[index]].satellite - 1
        start_times[satIndex].append(accessList[solution[index]].start_time)
        end_times[satIndex].append(accessList[solution[index]].end_time)
        weight = (satellite_weights[accessList[solution[index]].satellite-1]) / total_satellite_weight
        J1 -= weight * accessList[solution[index]].utility / accessList.max_utility
        J2 += weight * accessList[solution[index]].lost_imagery / accessList.max_lost_imagery

    J1 /= accessList.numUniqueSatellites
    J2 /= accessList.numUniqueSatellites
    
    #evaluate J3 - total time between accesses
    J3 = 0

    for i in range(accessList.numUniqueSatellites):
        min_start_index = start_times[i].index(min(start_times[i]))
        this_start = start_times[i].pop(min_start_index)
        this_end = end_times[i].pop(min_start_index)
        for j in range(len(start_times[i])):
            min_start_index = start_times[i].index(min(start_times[i]))
            next_start = start_times[i].pop(min_start_index)
            next_end = end_times[i].pop(min_start_index)
            J3 += next_start - this_end
            this_start = next_start
            this_end = next_end

    J3 /= -(accessList.max_time_delta * accessList.numUniqueSatellites)

    Jtotal = (J1 * cost_weights[0]) + (J2 * cost_weights[1]) + (J3 * cost_weights[2])
    Jtotal /= total_cost_weight
    
    if multiobjective:
        return [J1, J2, J3]
    else:
        return Jtotal

def benchmarkAlgorithms():
    """ benchmarkAlgorithms executes each OptimizerFunction algorithm a number of times and stores
        the results in text files (since the runtime is long), such that these results may be
        analyzed and plotted.
            return(list[list[float]]): the timing and objective value data for each algorithm
        """
    files = ['./results/benchmark/gaSingleObjectiveBenchmarkFile.out', './results/benchmark/gaMultiObjectiveBenchmarkFile.out',
            './results/benchmark/randomBenchmarkFile.out', './results/benchmark/randomWithGroupingBenchmarkFile.out']

    algorithms = [OptimizerFunction.GeneticAlgorithmSingleObjective, OptimizerFunction.GeneticAlgorithmMultiObjective,
                OptimizerFunction.Random, OptimizerFunction.RandomWithGrouping]
    
    caseCount = 10
    run_time = 300.0 #seconds

    results = [[],[],[],[]]

    #ga cases
    for i in range(0,2):
        try:
            with open(files[i], 'r') as myfile:
                data = myfile.read()
                data = data.split('|')
                data_x = data[0].replace("[","")
                data_x = data_x.replace("]","")
                dataSplit_x = data_x.split(",")
                data_y = data[1].replace("[","")
                data_y = data_y.replace("]","")
                dataSplit_y = data_y.split(",")
                for j in range(len(dataSplit_x)):
                    results[i].append([float(dataSplit_x[j]), float(dataSplit_y[j])])     
        except:
            history_list = []
            for j in range(caseCount):
                print('running '+str(algorithms[i])+' case '+str(j))
                optimize = Optimizer('sample_ground_accesses.csv', algorithms[i])
                initial_seed = generateSeed()       
                result = optimize.Optimize(run_time, initial_seed)
                history_list.append(optimize.history)

            results_sorted = [[],[]]

            times = []
            costs = []

            for j in range(caseCount):
                for k in range(len(history_list[j])):
                    times.append(history_list[j][k][0])
                    minCost = history_list[j][k][2][0].objectiveFunction(False)
                    for individual in history_list[j][k][2]:
                        minCost = min(minCost, individual.objectiveFunction(False))
                    costs.append(minCost)

            while len(times) > 0:
                minTime = min(times)
                minIndex = times.index(minTime)
                results_sorted[0].append(times.pop(minIndex))
                results_sorted[1].append(costs.pop(minIndex))

            with open(files[i], "w") as output:
                output.write(str(results_sorted[0])+'|'+str(results_sorted[1]))

    #random cases
    for i in range(2,4):
        try:
            with open(files[i], 'r') as myfile:
                data = myfile.read()
                data = data.split('|')
                data_x = data[0].replace("[","")
                data_x = data_x.replace("]","")
                dataSplit_x = data_x.split(",")
                data_y = data[1].replace("[","")
                data_y = data_y.replace("]","")
                dataSplit_y = data_y.split(",")
                for j in range(len(dataSplit_x)):
                    results[i].append([float(dataSplit_x[j]),float(dataSplit_y[j])])            
        except:
            history_list = []
            for j in range(caseCount):
                print('running '+str(algorithms[i])+' case '+str(j))
                optimize = Optimizer('sample_ground_accesses.csv', algorithms[i])
                initial_seed = generateSeed()       
                result = optimize.Optimize(run_time, initial_seed)
                history_list.append(optimize.history)

            results_sorted = [[],[]]

            times = []
            costs = []

            for j in range(caseCount):
                for k in range(len(history_list[j])):
                    times.append(history_list[j][k][0])
                    costs.append(history_list[j][k][2].cost)

            while len(times) > 0:
                minTime = min(times)
                minIndex = times.index(minTime)
                results_sorted[0].append(times.pop(minIndex))
                results_sorted[1].append(costs.pop(minIndex))

            with open(files[i], "w") as output:
                output.write(str(results_sorted[0])+'|'+str(results_sorted[1]))

    return results
