""" @package plots
    Package which is responsible for making pretty pictures

    This package utilizes matplotlib to present results to the user 
    in a graphical format.
"""

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import optimizer
from utils import *

def plot2DGAResults(gaHistory):
    """plot2DGAResults plots multiobjective GA results in a 2D plot (ignoring the time between accesses cost)
        in(optimizer.history): gaHistory: the generation and timing history from the GA
    """ 
    
    targetGenerations = [0, 10, 25, 50, 100]

    fig = plt.figure()
    ax1 = fig.add_subplot(111)
    for history in gaHistory:
        if history[1] in targetGenerations:
            color = np.random.rand(3)
            color[0] = 0.25 +(0.75 * (targetGenerations.index(history[1])/len(targetGenerations)))
            color[1] = 0
            color[2] = 0
            label = 'gen '+str(history[1])
            c = []
            x = []
            y = []
            for item in history[2]:
                if item.objectiveFunction(False) < 100:
                    x.append(item.objectiveFunction(True)[0])
                    y.append(item.objectiveFunction(True)[1])
                    c.append(color)
            ax1.scatter(x, y, s=5, color=c, alpha=1, label=label)
    plt.legend(loc='upper left')
    plt.xlabel('objective 1')
    plt.ylabel('objective 2')
    plt.title('Multiobjective GA Generations')
    plt.axis('equal')
    plt.show()

def plot3DGAResults(gaHistory):    
    """plot3DGAResults plots multiobjective GA results in a 3D plot 
        in(optimizer.history): gaHistory: the generation and timing history from the GA
    """ 
    targetGenerations = [0, 10, 25, 50, 100]

    fig = plt.figure()
    ax1 = fig.add_subplot(111, projection='3d')
    for history in gaHistory:
        if history[1] in targetGenerations:
            color = np.random.rand(3)
            color[0] = 0.25 +(0.75 * (targetGenerations.index(history[1])/len(targetGenerations)))
            color[1] = 0
            color[2] = 0
            label = 'gen '+str(history[1])
            c = []
            x = []
            y = []
            z = []
            for item in history[2]:
                if item.objectiveFunction(False) < 100:
                    x.append(item.objectiveFunction(True)[0])
                    y.append(item.objectiveFunction(True)[1])
                    z.append(item.objectiveFunction(True)[2])
                    c.append(color)
            ax1.scatter(x, y, z, s=5, color=c, alpha=1, label=label)
    plt.legend(loc='upper left')
    ax1.set_xlabel('objective 1')
    ax1.set_ylabel('objective 2')
    ax1.set_zlabel('objective 3')
    plt.title('Multiobjective GA Generations')
    plt.show()

def algorithmComparison():
    """algorithmComparison utilizes the results from optimizer.benchmarkAlgorithms to produce a
        time versus objective function line plot across all optimizer.OptimizerFunction options 
    """ 
    stats = optimizer.benchmarkAlgorithms()

    window_size = 50

    gaSingleObjectiveResults = stats[0]
    gaMultiobjectiveResults = stats[1]
    randomResults = stats[2]
    randomWithGroupingResults = stats[3]

    gaSingleObj_X = []
    gaSingleObj_Y = []
    gaMultiObj_X  = []
    gaMultiObj_Y  = []
    rand_X        = []
    rand_Y        = []
    randWGroup_X  = []
    randWGroup_Y  = []

    for entry in gaSingleObjectiveResults:
        gaSingleObj_X.append(entry[0])
        gaSingleObj_Y.append(entry[1])

    for entry in gaMultiobjectiveResults:
        gaMultiObj_X.append(entry[0])
        gaMultiObj_Y.append(entry[1])

    for entry in randomResults:
        rand_X.append(entry[0])
        rand_Y.append(entry[1])

    for entry in randomWithGroupingResults:
        randWGroup_X.append(entry[0])
        randWGroup_Y.append(entry[1])

    gaSingleObj_Fit = polyFit(gaSingleObj_X, gaSingleObj_Y, 4, 100)
    gaMultiObj_Fit = polyFit(gaMultiObj_X, gaMultiObj_Y, 4, 100)
    rand_Fit = polyFit(rand_X, rand_Y, 4, 100)
    randWGroup_Fit = polyFit(randWGroup_X, randWGroup_Y, 4, 100)

    plt.plot(rand_Fit[0], rand_Fit[1], label='Random')
    plt.plot(randWGroup_Fit[0], randWGroup_Fit[1], label='Random With Grouping')
    plt.plot(gaSingleObj_Fit[0], gaSingleObj_Fit[1], label='GA Single Objective')
    plt.plot(gaMultiObj_Fit[0], gaMultiObj_Fit[1], label='GA Multi Objective')
    plt.legend(loc='upper right')
    plt.ylim([-0.40,-0.27])
    plt.xlabel('time (s)')
    plt.ylabel('objective function')
    plt.title('Algorithm Comparison')
    plt.show()
