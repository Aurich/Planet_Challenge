""" @package main
    The head file for solving the provided problem.

    To solve the problem, simply execute ./python main.py.
    Boom, easy.
"""

from access import *
from unittests import *
sys.path.append('./optimization')
sys.path.append('./results')
import optimizer
import plots
import database
import numpy as np
import matplotlib.pyplot as plt

def runMain():
    """ The primary function which will be called upon program execution
    """
    version = 1.1
    optimizerFunction = optimizer.OptimizerFunction.GeneticAlgorithmMultiObjective

    #Read and format input file
    optimize = optimizer.Optimizer('sample_ground_accesses.csv', optimizerFunction)

    #Check unit tests for code and data consistency
    UnitTests(optimize.accessList)

    initial_seed = 1323569560
    run_time = 60.0 #seconds
    optimize.targetGeneration = 400

    result = optimize.Optimize(run_time, initial_seed)

    optimize.printOutputToFiles(result)

    database.postResults(version, optimize.accessList, result.solution, optimizerFunction)

    print(result.solution)
    print(result.cost)

if __name__ == '__main__':
    runMain()
