""" @package utils
    Package which keeps helper routines for miscellaneous needs, including the watchdog

    Generic help routines are kept here. The watchdog is utilized in the optimizer for keeping track of
    how long each iteration is taking, as well as signaling when to terminate the program.
"""

import time
import random
import sys
import numpy as np

def StandardTimeToSeconds(standardTime):
    """ Converts the time string from the input file into seconds since epoch
        in(string): standardTime: the input file time string
        return(double): time since epoch
    """
    timeSplit = standardTime.split(".")
    try:
        return time.mktime(time.strptime(timeSplit[0], '%Y-%m-%d %H:%M:%S'))+float("0."+str(timeSplit[1]))
    except:
        if standardTime is not '':
            print('Converting ' + standardTime + ' to seconds failed.')
        return None

def exists(item):
    """ returns if an item is equal to None, used for filtering lists
        in(type): item: checks if an item is None
        return(bool): true if the item exists
    """
    return (item is not None)

def intersect(range1, range2):
    """ Calculates if two ranges intersect
        in([float]): range1: the first range, composed of start and end
        in([float]): range2: the second range, composed of start and end
        return(bool): true if the ranges intersect
    """
    return ((range2[0] >= range1[0] and range2[0] <= range1[1]) or (range2[1] >= range1[0] and range2[1] <= range1[1]))

def generateSeed():
    """ Randomly generates a new seed
        return(int): a random seed
    """
    return int(random.random()*sys.maxsize)

def minIndex(generationList):
    """ Finds the index of the generation with the highest fitness
        in(list[generation]): generationList: a list of Generation objects
        return(int): the index of the generation with the highest fitness
    """
    minIndex = 0
    for i in range(len(generationList)):
        if generationList[i].cost < generationList[minIndex].cost:
            minIndex = i
    return minIndex

def simpleMovingAverage(x_list, window_size):
    """ Calculates the moving average of x_list with a window size of window_size
        in(list[float]): x_list: the list of values to be averaged
        in(int): window_size: the window size of the moving average
        return(list[float]): the moving average
    """
    size = len(x_list)
    cumsum = []
    averages = []

    for i in range(size):
        cumsum.append(x_list[i])
        if len(cumsum) > window_size:
            cumsum.pop(0)
        averages.append(sum(cumsum)/len(cumsum))
    return averages

def polyFit(xData, yData, degree, linspaceSize):
    """ Calculates the line of best fit to degree
        in(list[float]): x_list: the list of x values
        in(list[float]): xData: the list of y values
        in(int): degree: the order of the fit
        int(int): linspaceSize: the size of the linespace to plot
        return(numpy.array[[x],[y]]): the poly fit points
    """
    x = np.array(xData)
    y = np.array(yData)

    # calculate polynomial
    z = np.polyfit(x, y, degree)
    f = np.poly1d(z)

    # calculate new x's and y's
    x_new = np.linspace(x[0], x[-1], linspaceSize)
    y_new = f(x_new)

    return [x_new, y_new]

class Watchdog:
    """Documentation for the Watchdog.

    The watchdog is utilized in the optimizer, and wraps around the
    time package which is used to signal that the program should exit.
    """    

    def __init__(self, maxAllowedTime):
        """ The constructor.
            in(handle): self: handle of this object
            in(float): maxAllowedTime: how long until the program should exit
        """
        self.initial_time = 0
        self.MaximumElapsedTime = maxAllowedTime

    def start(self):
        """ Starts the timer
            in(handle): self: handle of this object
        """
        self.initial_time = time.time()

    def check(self):
        """ Checks if runtime has been exceeded
            out(bool): returns false if runtime has been exceeded
        """
        return (time.time() - self.initial_time) < self.MaximumElapsedTime

    def get_time(self):
        """ Returns the duration of the watchdog timer
            out(float): how long the watchdog timer has been running
        """
        return time.time() - self.initial_time
